import * as React from 'react';
import Box from '@mui/material/Box';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';


export const Loading = ({loading}:{loading:boolean}) => {
    return (
        <React.Fragment>
            <Dialog
                fullWidth={true}
                maxWidth="xs"
                open={loading}
                onClose={()=>{}}
                disableEscapeKeyDown={true}
                
            >
                <DialogTitle>Cargando...</DialogTitle>
                <DialogContent>
                    <Box sx={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center",position:"relative",height:"100px"}}>
                        <span className="loader" style={{marginBottom:"25px"}}></span>
                        <DialogContentText>
                            Por favor espere.
                        </DialogContentText>
                    </Box>
                </DialogContent>
            </Dialog>
        </React.Fragment>
    );
}
