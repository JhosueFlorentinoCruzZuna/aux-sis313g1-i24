import { getCharacters } from "@/service/RickAndMorty"
import { useEffect, useState } from "react"

export const useRickAndMorty = () => {
    const [characters,setCharacters] = useState<any>()
    const [loading,setLoading] = useState<boolean>(true)

    const fetchData = async () => {
        try{
            const responseCharacters = await getCharacters()
            setCharacters(responseCharacters)
            setLoading(false)
        }catch(e){
            console.log(e)
            setLoading(false)
        }
        
    }
    useEffect(() => {
        fetchData()
    },[])

    return {characters,loading}
}