export default function IconsLocal(props:{icono:string,title:string,parrafo:string}){
    return(
        <div className="icon" >
            <div>
                <img src={props.icono}/>
            </div>
            <div className="icon_text">
                <h3>{props.title}</h3>
                <p>{props.parrafo}</p>
            </div>
        </div>
    )
}