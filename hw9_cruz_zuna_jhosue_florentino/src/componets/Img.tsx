export default function Img(props:{a:string,b:string}){
    return(
        <div className="image">
            <img className="image1" src={props.a} />
            <img className="image2" src={props.b} />
        </div>
    )
}