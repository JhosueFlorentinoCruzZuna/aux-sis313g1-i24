export default function Title(props:{title:string,parr:string}){
    return(
        <div className="title">
            <h4 className="title1">{props.parr}</h4>
            <h2 className="title2">{props.title}</h2>
        </div>
    )
}