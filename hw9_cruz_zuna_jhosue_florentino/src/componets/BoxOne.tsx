import { title } from "process";
import char1 from '../../public/image/img_char.jpeg';
export default function BoxOne(props:{titleOne:string,titleTwo:string}){
    return(
    <div className="box-Image">
        <div >
            <div><h3><b>{props.titleOne}</b></h3></div> 
            <img src="char" />
            <div><p>{props.titleTwo}</p></div>
        </div>
    </div>
    )
}