import Image from "next/image";
import styles from "./page.module.css";
import Button from "@/componets/Button";
import BoxOne from "@/componets/BoxOne";
import Videos from "@/componets/Videos";
import IconsLocal from "@/componets/IconsLocal";
import Title from "@/componets/Title";
import Img from "@/componets/Img";
export default function Home() {
  return (
    <main className={styles.main}>
      <div className="hero">
        <div className="hero1">
          <div>
          <Title parr="Key features" title="We offer best services"></Title>
          <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature
from 45 BC.</p>
          </div>
          <IconsLocal icono="/image/icon_1.svg" title="We offer best services" parrafo="Lorem Ipsum is not simply random text"></IconsLocal>
          <IconsLocal icono="/image/icon_2.svg" title="Schedule your trip" parrafo="It has roots in a piece of classical"></IconsLocal>
          <IconsLocal icono="/image/icon_3.svg" title="Get discounted coupons" parrafo="Lorem Ipsum is not simply random text"></IconsLocal>
        </div>
        <div className="hero_img">
          <Img a="/image/primari_img.svg" b="/image/secund_img.svg"></Img>
        </div>
      </div>
    </main>
  )
}
